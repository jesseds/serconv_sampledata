# SERConv_sampledata

This repository contains the sample data for the SERConv project. This is separated
to prevent the main SERConv repository from being too large (as it is a simple program).
This sample data is required for the unittests.
